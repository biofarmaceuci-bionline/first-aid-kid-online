<?php
	session_start();

	require_once "DatabaseConnect.php";
	
	if(!isset($_SESSION['isLoggedIn']))
	{
		header('Location: index.php');
		exit();
	}
	
	
	$connect = @new mysqli($host, $db_user, $db_password, $db_name);

	if($connect->connect_errno!=0)
	{
		echo "Error: " . $connect->connect_errno;
	}
	
	include "GetOutdatedDrugs.php"; //alert

?>



<!DOCTYPE HTML>
<html lang='pl'> 

<head>
	<meta charset='utf-8' />
	<title> Wybierz lub dodaj apteczke </title>
	<meta name='description' content='apteczka' />
	<meta name='keywords' content='apteczka' />
	<meta http-equiv='X-UA-Compatible' content='IE-edge, chrome=1' />
	
	<link rel='stylesheet' href='style.css' type='text/css'/>
	
	
	<link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">

	
</head>


<body>
	
	<div id='container'> 
	
		<div class='rectangle'> 
			<div id='logo'> 
				Apteczka Internetowa 
			</div>
		
		</div>
		
		<div id='wybierzapteczke' class='square'> 
			<br/>
			Wybierz apteczkę:  <br/> <br/>
			<form method='post' action="leki.php">
				<select id='wyborapteczki' name='wyborapteczki'>
					<?php
						include "ListFirstAidKits.php";
					?>
				</select>
				<br>
				<input type='submit' value="Dalej">
				<br><br>
				<a href="Logout.php">Wyloguj się</a>
			</form>
		</div>
			
		<div id='przerwa' class='square'> 
			<br>
			<br>
		</div>
		
	
		<div id='dodajapteczke' class='square'> 
			<br/>
			Dodaj nową apteczke:
			<form action='addKit.php' method='post'>
				Nazwa apteczki: <input type='text' name='nazwaApteczki' placeholder="podaj nazwę" required> <br>
				<input type='submit' value="Dodaj"> <br/>
			</form>
			Dołącz do istniejącej apteczki:
			<form action='joinKit.php' method='post'>
				Istniejące apteczki: 
				<select id='wyborapteczki' name='wyborapteczki'>
					<?php
						include "ListNotJoinedFirstAidKits.php";
					?>
				</select>
				<br/>
				<input type='submit' value='Dołącz'> <br/>
			</form>
		</div>

		<div style="clear:both"> </div>
	
		<div class='square'> 
		

			<div id='howto' class='tile'> 
				How to
			</div> 
			
			<div id='projekt' class='tile'>
				O projekcie 
			</div> 
			
			<div id='onas' class='tile'> 
				O nas
			</div> </a>

			<div style='clear: both'> </div>
		
		</div>


		

		<div class='rectangle'> 
			2020 &copy; Monika Stachak & Olaf Tomaszewski
		</div>
		
		
	
	</div>


	
	
 
</body>

</html>