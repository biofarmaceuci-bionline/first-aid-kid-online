<?php
	session_start();

	require_once "DatabaseConnect.php";

	$_SESSION['idFirstAidKit'] = $_POST['wyborapteczki'];
	
	$mysqlConnection = @new mysqli($host, $db_user, $db_password, $db_name);
	
	if ($mysqlConnection->connect_errno != 0)
	{
		echo "Error: ".$mysqlConnection->connect_errno;
	}
	else

	//zamiana przecinkow na kropki w cenie leku podawanej
	$price = $_POST['cenaleku']; 
	$price = str_replace(",",".",$price);

	// $quantity = $_POST['iloscleku'];

	// $dateExpires = $_POST['terminwaznosci'];

	//require_once "addDrugs.php" jak bede miec zmienne

	$drugsQuery = "SELECT `id`, `NazwaHandlowa`  FROM `ListaLekow`";  


	$allDrugs = @$mysqlConnection->query($drugsQuery);
?>


<!DOCTYPE HTML>
<html lang='pl'> 

<head>
	<meta charset='utf-8' />
	<title> Leki </title>
	<meta name='description' content='apteczka' />
	<meta name='keywords' conte
	
	nt='apteczka' />
	<meta http-equiv='X-UA-Compatible' content='IE-edge, chrome=1' />
	
	<link rel='stylesheet' href='style.css' type='text/css'/>
	
	<link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">
</head>

<body>
	
	<div id='container'> 
	
		<div class='rectangle'> 
			<div id='logo'> 
				Apteczka Internetowa 
			</div>
		
		</div>

		<!-- formularz bedzie obsugiwany w innym skrypcie-->
		<div id='wybierzlek' class='square'> 
            <br/>
            Sprawdź informacje o stanie leków w Twojej apteczce
            <br> <br>
			Wybierz lek:  <br/> <br/>
			<form action='rozchodylekow.php' method='post' enctype="multipart/form-data">
				<select id='wyborleku1' name='wyborleku1'>
					<?php
						include "ListDrugs.php";
					?>
                </select>
                <input type='submit' value="Dalej">
			</form>
		</div>
			
		<div id='przerwa' class='square'> 
			<br>
			<br>
		</div>
		
		<!--DODAWANIE NOWEGO LEKU Z LISTY DO BAZY -->
		<div id='dodajlek' class='square'> 
			<br/>
			Dodaj nowy lek: <br/> <br/>
			<form action="addDrugs.php" method='post'>
				Nazwa leku:
				<?php
					echo "<select id='wyborleku2' name='wyborleku2'>";
					$i = 1;
					while(($row = mysqli_fetch_assoc($allDrugs)) && $i <= 200)
					{
						echo "<option value='".$row['id']."'>".$row['NazwaHandlowa']."</option>";
						$i++;
					}
					mysqli_free_result($allDrugs);
					echo "</select>";
				?>


				<br>
                Ilość opakowań: <input type='number' name='iloscleku' placeholder="podaj ilosc" min="1" required> <br/>
                Cena leku: <input type='number' name='cenaleku' placeholder="podaj cene w zł" min=0 step=".01" required> <br/>
                Termin ważności: <input type='date' name="terminwaznosci" required> <br/>
				<input type='submit' value="Dalej">
			</form>
		</div>

		<div style="clear:both"> </div>
	
		<div class='square'> 
		

			<div id='howto' class='tile'> 
				How to
			</div> 
			
			<div id='projekt' class='tile'>
				O projekcie 
			</div> 
			
			<div id='onas' class='tile'> 
				O nas
			</div> </a>

			<div style='clear: both'> </div>
		
		</div>


		

		<div class='rectangle'> 
			2020 &copy; Monika Stachak & Olaf Tomaszewski
		</div>
		
		
	
	</div>


	
	
 
</body>

</html>