<?php
	session_start();

	if(!isset($_SESSION['isLoggedIn']))
	{
		header('Location: index.php');
		exit();
	}

	require_once "DatabaseConnect.php";

	$mysqlConnection = @new mysqli($host, $db_user, $db_password, $db_name);
	
	if ($mysqlConnection->connect_errno != 0)
	{
		echo "Error: ".$mysqlConnection->connect_errno;
	}
	else	
	{
		$idFirstAidKit = $_POST['wyborapteczki'];
		$idUser = $_SESSION['idUser'];
		
		$query = "INSERT `UserFirstAidKitRelations` (`idFirstAidKit`, `idUser`) VALUES ('".$idFirstAidKit."', '".$idUser."')"; 
		
		$queryResult = $mysqlConnection->query($query);
		
		if ($queryResult == true)
		{
			$userRow = $queryResult->fetch_assoc();
		}
		
		$queryResult->close();
	}
	$mysqlConnection->close();
	header("Location: FirstAidKitChoice.php");

?>