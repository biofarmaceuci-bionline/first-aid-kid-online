<?php
	session_start();

	$_SESSION['idDrug'] = $_POST['wyborleku1'];
?>
<!DOCTYPE HTML>
<html lang='pl'> 

<head>
	<meta charset='utf-8' />
	<title> </title>
	<meta name='description' content='apteczka' />
	<meta name='keywords' content='apteczka' />
	<meta http-equiv='X-UA-Compatible' content='IE-edge, chrome=1' />
	
	<link rel='stylesheet' href='style.css' type='text/css'/>
	
	<link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">
</head>

<?php


?>

<body>
	
	<div id='container'> 
	
		<div class='rectangle'> 
			<div id='logo'> 
				Apteczka Internetowa 
			</div>
		
		</div>
		
		<div id='zuzycielekow' class='square'> 
            <br/>
            Informacje o leku w twojej apteczce:
			<br> 
			<?php
				include "getDrugInfo.php";
			?>
			<form action='' method='post' enctype="multipart/form-data">

				<input type="submit" value="Zażyj" formaction="reactionZazyj.php" />                       
				<input type="submit" value="Zutylizuj" formaction="reactionZutylizuj.php" />

			</form>
		</div>
			
		<div id='przerwa' class='square'> 
			<br>
			<br>
        </div>
        
        <div id='listautylizacji' class='square'> 
			<br/>
			Pamiętaj! <br> <br> Zawsze przed użyciem leku skonsultuj się z lekarzem lub farmaceutą!
		</div>

		<div style="clear:both"> </div>
		
	
	
		<div class='square'> 
		

			<div id='howto' class='tile'> 
				How to
			</div> 
			
			<div id='projekt' class='tile'>
				O projekcie 
			</div> 
			
			<div id='onas' class='tile'> 
				O nas
			</div> </a>

			<div style='clear: both'> </div>
		
		</div>


		

		<div class='rectangle'> 
			2020 &copy; Monika Stachak & Olaf Tomaszewski
		</div>
		
		
	
	</div>


</body>

</html>