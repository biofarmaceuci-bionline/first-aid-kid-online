<?php
	session_start();

	require_once 'DatabaseConnect.php';
		

	try
	{  
		$connect = new mysqli($host, $db_user,$db_password, $db_name);
			
		if($connect->connect_errno!=0)
		{
            throw new Exception(mysqli_connect_errno());
		}

		else
		{
			$drugsQuery = "SELECT NazwaHandlowa, idDrug FROM FirstAidKitDrugsRelations INNER JOIN ListaLekow ON FirstAidKitDrugsRelations.idDrug=ListaLekow.id WHERE idFirstAidKit = '". $_SESSION['idFirstAidKit'] ."'";
            $queryResult = $connect->query($drugsQuery);
            if(!$queryResult) throw new Exception($connect->error);
            else
			{
                while($row = mysqli_fetch_assoc($queryResult))
				{
                    echo "<option value='".$row['idDrug']."'>".$row['NazwaHandlowa']."</option>";
                }
            }
            $queryResult->free_result();
		}
        $connect->close();
	}
	catch(Exception $e)
	{
		echo "blad polaczenia z baza";
	}
?>
