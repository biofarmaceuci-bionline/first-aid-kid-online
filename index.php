<?php

	session_start();	

?>


<!DOCTYPE HTML>
<html lang='pl'> 

<head>
	<meta charset='utf-8' />
	<title> Strona startowa Domowa Apteczka </title>
	<meta name='description' content='apteczka' />
	<meta name='keywords' content='apteczka' />
	<meta http-equiv='X-UA-Compatible' content='IE-edge, chrome=1' />
	
	<link rel='stylesheet' href='style.css' type='text/css'/>
	
	
	<link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">
	
</head>

 <body>
	
	<div id='container'> 
	
		<div class='rectangle'> 
			<div id='logo'> 
				Apteczka Internetowa 
			</div>
		
		</div>
		
		<div id='zaloguj' class='square'> 
			<br/>
			Logowanie:  <br/> <br/>
			<form action="LoginAccount.php" method="post">
				Login: <input type='text' name='login' placeholder="podaj login" required> <br/>
				Hasło: <input type='password' name='password' placeholder="podaj hasło" required> <br/>
				<input type='submit' value="Dalej">
			<?php
				if(isset($SESSION['loginError'])) echo $SESSION['loginError'];
			?>
			</form>

		</div>
			
		<div id='przerwa' class='square'> 
			<br>
			<br>
		</div>
		
	
		<div id='zarejestruj' class='square'> 
			<br/>
			Rejestracja: <br/> <br/>
			<form action='RegisterAccount.php' method='post'>
				Email: <input type='email' name='registerEmail' placeholder="podaj email" required> <br>
				Login: <input type='text' name='registerLogin' placeholder="podaj login" required> <br>
				Hasło: <input type='password' name='registerPassword' placeholder="podaj hasło" required> <br/>
				<input type='submit' value="Dalej">
			</form>		
			<?php
				echo $SESSION['registerError'];
			?>
		</div>


		<div style="clear:both"> </div>
	
		<div class='square'> 
		

			<div id='howto' class='tile'> 
				How to
			</div> 
			
			<div id='projekt' class='tile'>
				O projekcie 
			</div> 
			
			<div id='onas' class='tile'> 
				O nas
			</div> </a>

			<div style='clear: both'> </div>
		
		</div>


		

		<div class='rectangle'> 
			2020 &copy; Monika Stachak & Olaf Tomaszewski
		</div>
		
		
	
	</div>
	
	
 
</body>

</html>