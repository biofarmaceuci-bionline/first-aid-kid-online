<?php
	session_start();

	require_once "DatabaseConnect.php";

	$mysqlConnection = @new mysqli($host, $db_user, $db_password, $db_name);
	
	if ($mysqlConnection->connect_errno != 0)
	{
		echo "Error: ".$mysqlConnection->connect_errno;
	}
	else
	{
		$login = $_POST['login'];
		$password = $_POST['password'];
		
		$loginQuery = "SELECT * FROM `Users` WHERE login='$login' AND password='$password'";
		
		$queryResult = @$mysqlConnection->query($loginQuery);
		
		if ($queryResult)
		{
			$detectedUsers = $queryResult->num_rows;
			
			if ($detectedUsers > 0)
			{
				$userRow = $queryResult->fetch_assoc();
				
				$_SESSION['isLoggedIn'] = true;
				$_SESSION['login'] = $userRow['login'];
				$_SESSION['idUser'] = $userRow['id'];
				
				unset($_SESSION['loginError']);
				
				
				header("Location: FirstAidKitChoice.php");
				
				$queryResult->close();
			}
			else 
			{
				$SESSION['loginError'] = '<span style="color:red">Błąd logowania.</br>Sprawdź login lub hasło ponownie.</span>';
				header("Location: index.php");
			}
			
		}
	}
	$mysqlConnection->close();
	
?>