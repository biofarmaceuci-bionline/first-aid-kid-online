<?php
	session_start();

	require_once 'DatabaseConnect.php';
	

	try
	{  
		$connect = new mysqli($host, $db_user,$db_password, $db_name);
			
		if($connect->connect_errno!=0)
		{
            throw new Exception(mysqli_connect_errno());
		}

		else
		{
			$outdatedFirstAidKitsQuery = "SELECT dateExpires, NazwaHandlowa FROM FirstAidKitDrugsRelations INNER JOIN ListaLekow ON FirstAidKitDrugsRelations.idDrug=ListaLekow.id WHERE dateExpires < NOW() AND idFirstAidKit in (SELECT idFirstAidKit FROM UserFirstAidKitRelations WHERE idUser = '". $_SESSION['idUser'] ."')";
            $queryResult = $connect->query($outdatedFirstAidKitsQuery);
            if(!$queryResult) throw new Exception($connect->error);
            else
			{
				$detectedDrugs = $queryResult->num_rows;
				if ($detectedDrugs > 0)
				{
					while($row = mysqli_fetch_assoc($queryResult))
					{
						$message = "Skończył się termin przydatności leku: " . $row['NazwaHandlowa'];
						echo "<script type='text/javascript'>alert('$message');</script>";
					}
				}
            }
            $queryResult->free_result();
		}
        $connect->close();
	}
	catch(Exception $e)
	{
    	echo "blad polaczenia z baza";
	}
?>
