<?php
	session_start();

	require_once 'DatabaseConnect.php';
		

	try
	{  
		$polaczenie = new mysqli($host, $db_user,$db_password, $db_name);
			
		if($polaczenie->connect_errno!=0)
		{
            throw new Exception(mysqli_connect_errno());
		}

		else
		{
			$kitquery = "SELECT `id`, `name` FROM `FirstAidKits` WHERE `id` in (SELECT `idFirstAidKit` FROM `UserFirstAidKitRelations` WHERE idUser = '". $_SESSION['idUser'] ."')";
            $rezultaty = $polaczenie->query($kitquery);
            if(!$rezultaty) throw new Exception($polaczenie->error);
            else
			{
                while($row = mysqli_fetch_assoc($rezultaty))
				{
                    echo "<option value='".$row['id']."'>".$row['name']."</option>";
                }
            }
            $rezultaty->free_result();
		}
        $polaczenie->close();
	}
	catch(Exception $e)
	{
		echo "blad polaczenia z baza";
	}
?>
